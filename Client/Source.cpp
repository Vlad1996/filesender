#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
#include <iostream>
#include <fstream>
#include <string>
#include<experimental/filesystem>
#include <WinSock2.h>
#include <ws2tcpip.h>
#include <locale>
#include <windows.h>

#pragma warning(disable:4996)
#pragma comment(lib, "ws2_32.lib")

#define PORT 1111

//send file
void send_file(SOCKET* sock, const std::string& file_name) {
	std::fstream file;

	file.open(file_name, std::ios_base::in | std::ios_base::binary);
	//check file is open or not
	if (!file.is_open())
	{
		std::cerr << "Error file open\n";
		exit(1);
		return;

	}
	//file name, size, data creating
	
	//int file_size = std::experimental::filesystem::file_size(file_name) + 1;
	int file_size = std::experimental::filesystem::file_size(file_name);

	std::string file_n = file_name;
	std::string file_s = std::to_string(file_size);

	// Data size cache
	const int packet_size = 524288;
	const int file_s_size = 16;
	const int file_n_size = 32;

	/*send(*sock, std::to_string(file_size).c_str(), 16, 0);
	Sleep(2000);

	send(*sock, file_name.c_str(), 32, 0);
	std::cout << std::flush;
	Sleep(2000);

	send(*sock, bytes, file_size, 0);*/

	if (file_s_size + file_n_size + file_size >= packet_size)
	{
		std::cerr << "File size too large.\n";
		exit(1);
		return;
	}

	char* bytes = new char[file_size];
	//read() function reads file_size bytes of input into the memory area indicated by bytes.
	file.read(bytes, file_size);

	//packet creating and initialization
	char* packet = new char[packet_size];
	int i = 0;
	for (; i < file_s_size; i++) {
		packet[i] = file_s[i];
	}
	for (int j = 0; j < file_n_size; j++, i++) {
		packet[i] = file_n[j];
	}
	for (int k = 0; k < file_size; k++, i++) {
		packet[i] = bytes[k];
	}
	//send packet
	send(*sock, packet, packet_size, 0);
	Sleep(10);

	//print data
	std::cout << "Size: " << file_size << std::endl;
	std::cout << "Name: " << file_name << std::endl;
	std::cout << "Data: " << bytes << std::endl;

	//geting ASCII cods data
	/*for (int i = 0; i < file_size; i++) {
		int a = bytes[i];
		std::cout << a << " ";
	}
	std::cout<<std::endl;*/

	delete[]packet;
	delete[]bytes;
	file.close();
}

//recv file get file with parametr socket
void recv_file(SOCKET* sock) {
	std::flush(std::cout);
	char file_size_str[16]{};
	char file_name[32]{};
	while (true) {
		//get file size
		//Sleep(10);
		int recv_result = recv(*sock, file_size_str, 16, 0);
		if (recv_result == SOCKET_ERROR) {
			return;
		}
		//file_size_str - is string atoi() makes int
		int file_size = atoi(file_size_str);

		if (file_size == 0) {
			//std::cerr << "File does not exist or file size to large.\n";
			//exit(1);
			continue;
		}
		//dinamic memory rezerve;
		char* bytes = new char[file_size];
		//get file name
		recv(*sock, file_name, 32, 0);

		//Sleep(10);
		std::fstream file;
		file.open(file_name, std::ios_base::out | std::ios_base::binary);

		//Print file_size, file_name;
		std::cout << "size: " << file_size << std::endl;
		std::cout << "name: " << file_name << std::endl;
		//check file opening
		if (!file.is_open()) {
			std::cerr << "Error file open\n";
		}
		//get file data
		recv(*sock, bytes, file_size, 0);
		//print data
		std::cout << "data: " << bytes << std::endl;
		//write bytes
		file.write(bytes, file_size);
		std::cout << "ok save\n";

		delete[]bytes;
		file.close();
		
	}
}


int main() {
	//initialize WinSock
	WSAData wsaData;
	WORD DLLVersion = MAKEWORD(2, 1);
	//start Socket
	int InitState = WSAStartup(DLLVersion, &wsaData);
	if (InitState) {
		std::cerr << "Error" << std::endl;
		return 1;
	}

	//info address socket
	SOCKADDR_IN addr;
	int sizeofaddr = sizeof(addr);
	addr.sin_addr.s_addr = inet_addr("172.16.255.26");	
	addr.sin_port = htons(PORT);
	addr.sin_family = AF_INET;
	
	 SOCKET Connection = socket(AF_INET, SOCK_STREAM, NULL);
	 int connectResult = connect(Connection, (SOCKADDR*)&addr, sizeofaddr);
	
		if (connectResult != 0) {
				std::cout << "Error: failed connect to server. \n";
				return 1;
		}
		std::cout << "Connected\n";
		CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)recv_file, (LPVOID)(&Connection), NULL, NULL);
		//call function recv_file with parametr & socket
		
		while (true) {
			std::string path;
			std::cin >> path;
			std::flush(std::cout);
			send_file(&Connection, path);
		}
	
		return 0;
	}