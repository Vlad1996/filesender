﻿#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
#include <iostream>
#include <fstream>
#include <string>
#include<experimental/filesystem>
#include <WinSock2.h>

#pragma warning(disable:4996)
#pragma comment(lib, "ws2_32.lib")

//send file function with parametres socket and file_name;

void send_file(SOCKET* sock, const std::string& file_name) {
	std::fstream file;
	file.open(file_name, std::ios_base::in | std::ios_base::binary);
	std::string file_N = (file_name);
	//review if file open...

	if (file.is_open()) {
		int file_size = std::experimental::filesystem::file_size(file_name);
		
		std::string file_n = file_name;
		std::string file_s = std::to_string(file_size);
		
		char* bytes = new char[file_size];
		file.read(bytes, file_size);
		//cout in console server

		std::cout << "Size: " << file_size << std::endl;
		std::cout << "Name: " << file_N << std::endl;
		std::cout << "Data: " << bytes << std::endl;
		
		//send file_size, file_name, data;
		const int packet_size = 4096;
		const int file_s_size = 16;
		const int file_n_size = 32;

		char* packet=new char[packet_size];

		int i = 0;
		for (; i < file_s_size; i++) {
			packet[i] = file_s[i];
		}
		for (int j = 0; j < file_n_size; j++,i++) {
			packet[i] = file_n[j];
		}
		for (int k = 0; k < file_size; k++, i++) {
			packet[i] = bytes[k];
		}
		
		//packet[0] = std::to_string(file_size).c_str(), 16, 0;
		////std::cout << packet[0] << std::endl;
		////packet[1] = file_name.c_str(), 32, 0;
		//packet[1] = file_N.c_str(), 32, 0;
		////std::cout << packet[1] << std::endl;
		//packet[2] = bytes, file_size, 0;
		////std::cout << packet[2] << std::endl;]

		/*send(*sock, std::to_string(file_size).c_str(), 16, 0);
		send(*sock, file_name.c_str(), 32, 0);
		send(*sock, bytes, file_size, 0);*/

		Sleep(100);
			send(*sock, packet, packet_size, 0);
			std::cout << packet << std::endl;

		//for (int i = 0; i < 3; i++) {
		//	//send(*sock,packet[i], sizeof(packet[i]), 0);
		//	send(*sock,packet[i], packet_size, 0);
		//	//Sleep(20);
		//}

		delete[]packet;
		delete[]bytes;
	}
	else {
		std::cout << "Error file open\n";
	}

}

//void send_file(SOCKET* sock, const std::string& file_name) {
//	std::fstream file;
//	file.open(file_name, std::ios_base::in | std::ios_base::binary);
//
//	//review if file open...
//
//	if (file.is_open()) {
//		int file_size = std::experimental::filesystem::file_size(file_name) + 1;
//		char* bytes = new char[file_size];
//
//		file.read(bytes, file_size);
//
//		//cout in console server
//
//		//std::cout << "Size: " << file_size << std::endl;
//		//std::cout << "Name: " << file_name << std::endl;
//		//std::cout << "Data: " << bytes << std::endl;
//
//		//send file_size, file_name, data;
//
//
//
//		/*send(*sock, std::to_string(file_size).c_str(), 16, 0);
//		send(*sock, file_name.c_str(), 32, 0);
//		send(*sock, bytes, file_size, 0);
//		*/
//
//		std::string packet = "Hello, client!";
//		send(*sock, bytes, 1024, 0);
//		Sleep(2000);
//		delete[]bytes;
//		
//	}
//	else {
//		std::cout << "Error file open\n";
//	}
//
//}


int main() {
	//Инициализация Winsock
	WSAData wsaData;
	//запрашивает версию 2.2 Winsock в системе и задает переданную версию в качестве
	//самой высокой версии Windows Sockets поддержки, которую может использовать 
	//вызывающий объект.
	WORD DLLVersion = MAKEWORD(2, 1);

	//запуска использования WS2_32.dll

	WSAStartup(DLLVersion, &wsaData);
	if (WSAStartup(DLLVersion, &wsaData) != 0) {
		std::cout << "Error" << std::endl;
		exit(1);
	}

	//info address socket

	SOCKADDR_IN addr;
	int sizeofaddr = sizeof(addr);
	//для ip - адреса в структуре адресов сокета 
	//устанавливается значение INADDR_ANY для IPv4 - адресов
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(1111);
	//AF_INET используется для указания семейства адресов IPv4.
	addr.sin_family = AF_INET;

	SOCKET sListen = socket(AF_INET, SOCK_STREAM, NULL);
	//Привязка сокета
	bind(sListen, (SOCKADDR*)&addr, sizeof(addr));
	//Подготавливает сервер к входящим клиентским запросам
	listen(sListen, SOMAXCONN);
	//принятия запроса на подключение от клиента.
	 SOCKET newConnection = accept(sListen, (SOCKADDR*)&addr, &sizeofaddr);

		if (newConnection != 0) {
			std::cout << "Connect\n";
			std::string path;
			std::cin >> path;
			send_file(&newConnection, path);
		}
	
	return 0;
}